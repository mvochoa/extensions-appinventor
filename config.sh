#/bin/bash

properties=Extensions/lib/nblibraries.properties
classpath="../appinventor-sources/appinventor/components/src"

for file in appinventor-sources/appinventor/lib/**/*.jar
do
    classpath+=":../$file"
done

for file in appinventor-sources/appinventor/lib/**/**/*.jar
do
    classpath+=":../$file"
done

echo "javac.classpath=$classpath" > $properties